import { get, post, put, deleteData } from "./core/Requests";

export const getBuses = async () => {
  return await get(`/Bus/GetBuses`);
};

export const getBusDetail = async (busId) => {
  return await get(`/Bus/detail/${busId}`);
};

export const getBusDetailForMap = async (busId) => {
  return await get(`/Bus/detailForMap/${busId}`);
};

// // export const getPostsByOrganization = async ( pageIndex, pageSize) => {
// //   return await get(`/Post/getByOrganization`, {
// //     pageIndex,
// //     pageSize,
// //   });
// // };

// export const getForSelect = async () => {
//   return await get(`/Post/getForSelect`);
// };

// export const getPost = async (id) => {
//   return await get(`/Post/${id}`);
// };

// export const addPost = async (data) => {
//   return await post(`/Post`, data);
// };

// export const updatePost = async (data) => {
//   return await put(`/Post`, data);
// };

// export const deletePost = async (id) => {
//   return await deleteData(`/Post/${id}`);
// };

// export const changeActive = async (id) => {
//   return await get(`/Post/changeActive/${id}`);
// };
