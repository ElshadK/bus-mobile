import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Pressable,
  TextInput,
  ScrollView,
} from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import Loading from './../../components/loading/Index';
import BusItem from "./../../components/busItem/Index";

import * as busService from "./../../services/BusService";

export default function Index({navigation}) {
  const [text, onChangeText] = useState("");
  const [buses, setBuses] = useState([]);

  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: "Apple", value: "apple" },
    { label: "Banana", value: "banana" },
  ]);

  useEffect(() => {
    console.log("busss");
    getBuses();
  }, []);

  const getBuses = async () => {
    let result = await busService.getBuses();
    setBuses(result);
  };

  return (
    <ScrollView>
      {/* <DropDownPicker
        open={open}
        value={value}
       // items={items}
        setOpen={setOpen}
       // setValue={setValue}
     //   setItems={setItems}
        searchable={true}
        loading={loading}
        searchContainerStyle={{
            borderBottomColor: "#dfdfdf"
          }}
        disableLocalSearch={true} // required for remote search
        onChangeSearchText={(text) => {
          // Show the loading animation
          setLoading(true);
          //setItems={setItems}
          setLoading(false);

          // Get items from API
        //   API.get("/items/search", {
        //     text,
        //   })
        //     .then((items) => {
        //       setItems(items);
        //     })
        //     .catch((err) => {
        //       //
        //     })
        //     .finally(() => {
        //       // Hide the loading animation
        //       setLoading(false);
        //     });
        }}
      /> */}

      {buses.length > 0 ? buses?.map((bus, i) => <BusItem key={i} bus={bus} />) : <Loading/>}

    </ScrollView>
  );
}

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: "white",
    margin: 2,
    padding: 8,
    borderTopEndRadius: 35,
    borderBottomEndRadius: 35,
    //    borderRadius: 35,

    Height: 70,
    flexDirection: "row",
    elevation: 5,
  },
  number: {
    color: "white",
    backgroundColor: "#CE1212",
    width: 60,
    height: 60,
    fontWeight: "bold",
    fontSize: 18,
    //padding: 20,
    borderRadius: 30,
    textAlign: "center",
    textAlignVertical: "center",
  },
  numberContainer: {
    alignItems: "flex-end",
    justifyContent: "center",
    flex: 1,
  },
  from: {
    color: "black",
    fontSize: 18,
    paddingLeft: 10,
  },
  to: {
    fontSize: 18,
    paddingLeft: 10,
  },

  input: {
    //height: 40,
    // margin: 5,
    borderRadius: 5,
    //  borderWidth: 1,
    padding: 10,
    backgroundColor: "white",
    elevation: 5,
  },
});
