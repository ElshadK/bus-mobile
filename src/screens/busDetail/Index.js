import React, { useState, useEffect } from "react";
import { ScrollView, Text, StyleSheet, View } from "react-native";
import * as busService from "./../../services/BusService";
import BusItem from "./../../components/busItem/Index";
import MapView, { Marker, Polyline } from "react-native-maps";
import Loading from "./../../components/loading/Index";
import Tab from "./../../components/tab/Index";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import ActionButton from "react-native-action-button";

export default function Index({ route }) {
  const { busId } = route.params;
  const [busDetail, setBusDetail] = useState(null);
  const [busDetailForMap, setBusDetailForMap] = useState(null);
  const [tab, setTab] = useState("forward");
  const [type, setType] = useState("list");

  useEffect(() => {
    getBusDetail();
    getBusDetailForMap();
  }, []);

  const getBusDetail = async () => {
    let result = await busService.getBusDetail(busId);
    if (result.backward == null && result.forward == null) {
      setTab("circular");
    }
    setBusDetail(result);
  };

  const getBusDetailForMap = async () => {
    let result = await busService.getBusDetailForMap(busId);
    if (result.backward == null && result.forward == null) {
      setTab("circular");
    }
    setBusDetailForMap(result);
  };

  return (
    <>
      <View style={{ flex: 1, paddingTop: 10 }}>
        {busDetail != null ? (
          <>
            <BusItem bus={busDetail?.bus} />
            <Tab tab={tab} setTab={setTab} />

            {type == "map" ? (
              <MapView
                style={styles.map}
                initialRegion={{
                  latitude: busDetailForMap?.center?.latitude,
                  longitude: busDetailForMap?.center?.longitude,
                  latitudeDelta: 0.2,
                  longitudeDelta: 0.07,
                }}
              >
                <Polyline
                  coordinates={busDetailForMap[tab]?.polylines}
                  strokeColor="blue"
                  strokeWidth={5}
                />
                {busDetailForMap[tab]?.platforms?.map((platform, i) => (
                  <Marker
                    key={i}
                    coordinate={platform}
                    title={busDetail[tab]?.platforms[i]?.name}
                  >
                    <MaterialCommunityIcons
                      name="map-marker"
                      size={25}
                      color="darkred"
                    />
                  </Marker>
                ))}
              </MapView>
            ) : (
              <ScrollView>
                {busDetail[tab]?.platforms?.map((platform, i) => (
                  <View key={i} style={styles.listItem}>
                    <MaterialCommunityIcons
                      name="bus-marker"
                      size={25}
                      color="green"
                    />
                    <Text style={styles.listText}>{platform?.name}</Text>
                  </View>
                ))}
              </ScrollView>
            )}
          </>
        ) : (
          <Loading />
        )}
        <ActionButton
          size={70}
          buttonColor="#2666CF"
          onPress={() => setType(type == "map" ? "list" : "map")}
          renderIcon={() => (
            <MaterialCommunityIcons
              name={type == "map" ? "format-list-bulleted" : "map-legend"}
              size={30}
              color="white"
            />
          )}
        ></ActionButton>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  map: {
    minHeight: "90%",
    width: "100%",
  },
  listItem: {
    flexDirection: "row",
    padding: 10,
    backgroundColor: "white",
    elevation: 5,
  },
  listText: { fontSize: 17, color: "black", padding: 3 },
});
