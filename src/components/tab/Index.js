import React, { useState, useEffect } from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";

export default function Index({tab, setTab}) {
  return (
    <View style={{ flexDirection: "row", width: "100%", paddingTop: 10 }}>
      {tab == "circular" ? (
        <TouchableOpacity style={styles.btnTabSelect}>
          <Text style={styles.txtTabSelect}>Dairəvi</Text>
        </TouchableOpacity>
      ) : (
        <>
          <TouchableOpacity
            style={tab == "forward" ? styles.btnTabSelect : styles.btnTab}
            onPress={() => setTab("forward")}
          >
            <Text
              style={tab == "forward" ? styles.txtTabSelect : styles.txtTab}
            >
              İləri
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={tab == "backward" ? styles.btnTabSelect : styles.btnTab}
            onPress={() => setTab("backward")}
          >
            <Text
              style={tab == "backward" ? styles.txtTabSelect : styles.txtTab}
            >
              Geri
            </Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  listItem: { padding: 10, backgroundColor: "white", elevation: 5 },
  listText: { fontSize: 17, color: "black" },
  btnTabSelect: {
    backgroundColor: "green",
    flex: 1,
    padding: 15,
    elevation: 5,
  },
  btnTab: {
    backgroundColor: "white",
    flex: 1,
    padding: 15,
    elevation: 5,
    borderBottomColor: "green",
    borderBottomWidth: 2,
  },

  txtTabSelect: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
    fontWeight: "bold",
  },
  txtTab: {
    color: "black",
    fontSize: 15,
    textAlign: "center",
    fontWeight: "bold",
  },
});
