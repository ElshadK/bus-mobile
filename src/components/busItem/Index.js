import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, Pressable } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function Index({ bus }) {

    const navigation = useNavigation();

  return (
    <Pressable onPress={() => navigation.navigate("BusDetail", {busId: bus?.busId})}>
      <View style={styles.listItem}>
        <View style={{ justifyContent: "center", width: "82%" }}>
          <Text style={styles.from}>{bus?.from_name}</Text>
          <Text style={styles.to}>{bus?.to_name}</Text>
        </View>
        <View style={styles.numberContainer}>
          <Text style={styles.number}>{bus?.name}</Text>
        </View>
      </View>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: "white",
    margin: 2,
    padding: 8,
    borderTopEndRadius: 35,
    borderBottomEndRadius: 35,
    //    borderRadius: 35,

    Height: 70,
    flexDirection: "row",
    elevation: 5,
  },
  number: {
    color: "white",
    backgroundColor: "#CE1212",
    width: 60,
    height: 60,
    fontWeight: "bold",
    fontSize: 18,
    //padding: 20,
    borderRadius: 30,
    textAlign: "center",
    textAlignVertical: "center",
  },
  numberContainer: {
    alignItems: "flex-end",
    justifyContent: "center",
    flex: 1,
  },
  from: {
    color: "black",
    fontSize: 18,
    paddingLeft: 10,
  },
  to: {
    fontSize: 18,
    paddingLeft: 10,
  },
});
